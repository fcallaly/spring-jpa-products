package com.citi.training.products.rest;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.products.model.Product;
import com.citi.training.products.repo.ProductRepository;
import com.fasterxml.jackson.databind.ObjectMapper;


@RunWith(SpringRunner.class)
@WebMvcTest(ProductsController.class)
public class ProductControllerTests {

    private static final Logger logger = LoggerFactory.getLogger(ProductControllerTests.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductRepository mockProductRepo;

    @Test
    public void findAllProducts_returnsList() throws Exception {
        when(mockProductRepo.findAll()).thenReturn(new ArrayList<Product>());

        MvcResult result = this.mockMvc.perform(get("/products")).andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber()).andReturn();

        logger.info("Result from ProductDao.findAll: " +
                    result.getResponse().getContentAsString());
    }


    @Test
    public void getProductById_returnsOK() throws Exception {
        Product testProduct = new Product(1, "John", 23.3);

        when(mockProductRepo.findById(testProduct.getId())).thenReturn(
                                                    Optional.of(testProduct));

        MvcResult result = this.mockMvc.perform(get("/products/1"))
                                       .andExpect(status().isOk()).andReturn();

        logger.info("Result from ProductDao.getProduct: " +
                    result.getResponse().getContentAsString());
    }

    @Test
    public void createProduct_returnsOk() throws Exception {
        Product testProduct = new Product(5, "Bob", 99999.99);

        this.mockMvc
                .perform(post("/products").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(testProduct)))
                .andExpect(status().isOk()).andReturn();
        logger.info("Result from Create Product");
    }

}
