package com.citi.training.products.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.products.model.Product;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2")
public class ProductControllerIntegrationTests {

    private static final Logger logger = LoggerFactory.getLogger(
                                            ProductControllerIntegrationTests.class);

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getProduct_returnsProduct() {
        restTemplate.postForEntity("/products", new Product(-1, "Coffee", 123.23), Product.class);

        ResponseEntity<List<Product>> getAllResponse = restTemplate.exchange(
                "/products", HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Product>>() {});

        logger.info("getAllProduct response: " + getAllResponse.getBody());

        assertEquals(HttpStatus.OK, getAllResponse.getStatusCode());
        assertTrue(getAllResponse.getBody().get(0).getName().equals("Coffee"));
        assertEquals(getAllResponse.getBody().get(0).getPrice(), 123.23, 0.0001);
    }
}
