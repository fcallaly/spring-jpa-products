package com.citi.training.products.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.products.model.Product;
import com.citi.training.products.repo.ProductRepository;

@RestController
@RequestMapping("/products")
public class ProductsController {

    @Autowired
    ProductRepository productRepo;

    @RequestMapping(method=RequestMethod.GET)
    public Iterable<Product> findAll() {
        return productRepo.findAll();
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Product findById(@PathVariable long id) {
        return productRepo.findById(id).get();
    }

    @RequestMapping(method=RequestMethod.POST)
    public Product save(@RequestBody Product product) {
        return productRepo.save(product);
    }
}
