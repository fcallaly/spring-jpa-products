package com.citi.training.products.repo;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.products.model.Product;

public interface ProductRepository extends CrudRepository<Product, Long> { }
